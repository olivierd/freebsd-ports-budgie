# FreeBSD Budgie desktop development repo

> Due to bug [#275899](https://bugs.freebsd.org/bugzilla/show_bug.cgi?id=275899), this repository will no longer receive updates.
>
> The last version of `x11/budgie-desktop` is **10.9.1**.

This is [ports](https://cgit.freebsd.org/ports/) collection in order to run this desktop.

```
# pkg install budgie
```

For complete documentation, go to the [wiki](../../../wiki).
