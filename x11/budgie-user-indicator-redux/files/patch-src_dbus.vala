--- src/dbus.vala.orig	2023-02-04 19:01:54 UTC
+++ src/dbus.vala
@@ -13,7 +13,7 @@
 using GLib;
 
 namespace UserIndicatorRedux {
-    [DBus (name = "org.freedesktop.login1.Manager")]
+    [DBus (name = "org.freedesktop.ConsoleKit.Manager")]
     public interface LogindInterface : Object {
         public abstract string can_hibernate () throws DBusError, IOError;
         public abstract void suspend (bool interactive) throws DBusError, IOError;
