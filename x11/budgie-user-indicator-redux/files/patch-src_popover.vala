--- src/popover.vala.orig	2023-02-04 19:01:54 UTC
+++ src/popover.vala
@@ -246,7 +246,7 @@ namespace UserIndicatorRedux {
 
         private async void init_interfaces () {
             try {
-                logind_interface = yield Bus.get_proxy<LogindInterface> (BusType.SYSTEM, "org.freedesktop.login1", "/org/freedesktop/login1");
+                logind_interface = yield Bus.get_proxy<LogindInterface> (BusType.SYSTEM, "org.freedesktop.ConsoleKit", "/org/freedesktop/ConsoleKit/Manager");
             } catch (Error e) {
                 warning ("Unable to connect to LoginD interface: %s", e.message);
             }
